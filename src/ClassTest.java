import java.util.Scanner;

public class ClassTest {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Enter a string type word: ");
        String string;
        string  = scanner.nextLine();
        int limit = 0;
        while (true){
            if(limit==10){
                break;
            }
            limit++;
            System.out.println(isAbecedarian(string));
            System.out.print("Enter a string type word: ");
            string  = scanner.nextLine();
        }
    }

    public static boolean isAbecedarian(String string) {
        boolean checking = false;
        for (int index = 0; index < string.length() - 1; index++) {
            if (string.charAt(index) <= string.charAt(index + 1)) {
                checking = true;
            }
            else {
                return false;
            }
        }
        return checking;
    }
}
